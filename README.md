Go package for encrypting and decrypting text. Currently supports AES encryption.

Get:
```
go get -u gitlab.com/maikeme/encrypt
```

Use:
```
package main

import (
	"fmt"
	"gitlab.com/maikeme/encrypt"
)

func init() {
	err := encrypt.SetAESCipherKey("MySuperSecretKeyWith32Characters")
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	encryptedText, err := encrypt.AESEncryptPlaintext([]byte("Hello World"))
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("encryptedText:", string(encryptedText))

	decryptedText, err := encrypt.AESDecryptCiphertext(encryptedText)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("decryptedText:", string(decryptedText))
}
```