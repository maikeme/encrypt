package encrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"io"
)

var cipherKey []byte

func SetAESCipherKey(key string) error {
	if len(key) != 16 && len(key) != 24 && len(key) != 32 {
		return errors.New("Invalid key size, please use 16, 24 or 32 characters")
	}
	cipherKey = []byte(key)
	return nil
}

func AESEncryptPlaintext(plaintext []byte) ([]byte, error) {
	c, err := aes.NewCipher(cipherKey)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func AESDecryptCiphertext(ciphertext []byte) ([]byte, error) {
	c, err := aes.NewCipher(cipherKey)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}
